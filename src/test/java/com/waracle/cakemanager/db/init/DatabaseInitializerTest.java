package com.waracle.cakemanager.db.init;

import com.waracle.cakemanager.db.CakeRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class DatabaseInitializerTest {

    private static final int EXPECTED_DATABASE_ENTRY_COUNT = 3;

    @Autowired
    DatabaseInitializer databaseInitializer;

    @Autowired
    CakeRepository cakeRepository;

    @Test
    @DisplayName("Testing init with valid json file adds all valid Cake objects to the database ignoring duplicate entries")
    void testInitWithValidJsonFile() throws Exception {
        //given + when
        databaseInitializer.init(cakeRepository).run();

        //then
        assertEquals(EXPECTED_DATABASE_ENTRY_COUNT, cakeRepository.findAll().size());
    }
}
