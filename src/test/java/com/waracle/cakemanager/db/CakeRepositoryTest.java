package com.waracle.cakemanager.db;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static com.waracle.cakemanager.DataFixture.*;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class CakeRepositoryTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private CakeRepository cakeRepository;

    @Test
    @DisplayName("Testing save successfully adds a Cake to the table")
    void testSave() {
        //given + when
        cakeRepository.save(TEST_CAKE_1);

        //then
        Optional<Cake> storedCake = cakeRepository.findById(1);
        assertTrue(storedCake.isPresent());
        assertEquals(TEST_CAKE_1, storedCake.get());
    }

    @Test
    @DisplayName("Testing save fails when Cake with duplicate title already exists in table")
    void testSaveFailsWhenDuplicateTitleExists() {
        //given + when
        cakeRepository.save(TEST_CAKE_1);

        //then
        assertThrows(DataIntegrityViolationException.class, () -> cakeRepository.save(TEST_CAKE_1));
    }

    @Test
    @DisplayName("Testing save fails validation when Cake with null title is added")
    void testSaveFailsWhenCakeHasNullTitle() {
        //given + when + then
        assertThrows(ConstraintViolationException.class, () -> cakeRepository.save(new Cake(null, TEST_CAKE_1_DESCRIPTION, TEST_CAKE_1_IMAGE)));
    }

    @Test
    @DisplayName("Testing save fails validation when Cake with empty title is added")
    void testSaveFailsWhenCakeHasEmptyTitle() {
        //given + when + then
        assertThrows(ConstraintViolationException.class, () -> cakeRepository.save(new Cake("", TEST_CAKE_1_DESCRIPTION, TEST_CAKE_1_IMAGE)));
    }

    @Test
    @DisplayName("Testing save fails validation when Cake with title exceeding max length is added")
    void testSaveFailsWhenCakeHasTitleThatExceedsMaxLength() {
        //given
        String stringGreaterThan100Chars = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

        //when + then
        assertThrows(ConstraintViolationException.class, () -> cakeRepository.save(new Cake(stringGreaterThan100Chars, TEST_CAKE_1_DESCRIPTION, TEST_CAKE_1_IMAGE)));
    }

    @Test
    @DisplayName("Testing save fails validation when Cake with null description is added")
    void testSaveFailsWhenCakeHasNullDescription() {
        //given + when + then
        assertThrows(ConstraintViolationException.class, () -> cakeRepository.save(new Cake(TEST_CAKE_1_TITLE, null, TEST_CAKE_1_IMAGE)));
    }

    @Test
    @DisplayName("Testing save fails validation when Cake with empty description is added")
    void testSaveFailsWhenCakeHasEmptyDescription() {
        //given + when + then
        assertThrows(ConstraintViolationException.class, () -> cakeRepository.save(new Cake(TEST_CAKE_1_TITLE, "", TEST_CAKE_1_IMAGE)));
    }

    @Test
    @DisplayName("Testing save fails validation when Cake with title exceeding max length is added")
    void testSaveFailsWhenCakeHasDescriptionThatExceedsMaxLength() {
        //given
        String stringGreaterThan100Chars = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

        //when + then
        assertThrows(ConstraintViolationException.class, () -> cakeRepository.save(new Cake(TEST_CAKE_1_TITLE, stringGreaterThan100Chars, TEST_CAKE_1_IMAGE)));
    }

    @Test
    @DisplayName("Testing save fails validation when Cake with null image is added")
    void testSaveFailsWhenCakeHasNullImage() {
        //given + when + then
        assertThrows(ConstraintViolationException.class, () -> cakeRepository.save(new Cake(TEST_CAKE_1_TITLE, TEST_CAKE_1_DESCRIPTION, null)));
    }

    @Test
    @DisplayName("Testing save fails validation when Cake with empty image is added")
    void testSaveFailsWhenCakeHasEmptyImage() {
        //given + when + then
        assertThrows(ConstraintViolationException.class, () -> cakeRepository.save(new Cake(TEST_CAKE_1_TITLE, TEST_CAKE_1_DESCRIPTION, "")));
    }

    @Test
    @DisplayName("Testing save fails validation when Cake with image exceeding max length is added")
    void testSaveFailsWhenCakeHasImageThatExceedsMaxLength() {
        //given
        String stringGreaterThan100Chars = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        String stringGreaterThan300Chars = stringGreaterThan100Chars + stringGreaterThan100Chars + stringGreaterThan100Chars;

        //when + then
        assertThrows(ConstraintViolationException.class, () -> cakeRepository.save(new Cake(TEST_CAKE_1_TITLE, TEST_CAKE_1_DESCRIPTION, stringGreaterThan300Chars)));
    }
}
