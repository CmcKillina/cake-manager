package com.waracle.cakemanager;

import com.waracle.cakemanager.db.Cake;

public final class DataFixture {

    public static final String TEST_CAKE_1_TITLE = "testCake1Title";
    public static final String TEST_CAKE_1_DESCRIPTION = "testCake1Description";
    public static final String TEST_CAKE_1_IMAGE = "testCake1Image";
    public static final Cake TEST_CAKE_1 = new Cake(TEST_CAKE_1_TITLE, TEST_CAKE_1_DESCRIPTION, TEST_CAKE_1_IMAGE);

    public static final String TEST_CAKE_2_TITLE = "testCake2Title";
    public static final String TEST_CAKE_2_DESCRIPTION = "testCake2Description";
    public static final String TEST_CAKE_2_IMAGE = "testCake2Image";
    public static final Cake TEST_CAKE_2 = new Cake(TEST_CAKE_2_TITLE, TEST_CAKE_2_DESCRIPTION, TEST_CAKE_2_IMAGE);

    private DataFixture() {
        //Utility class
    }
}
