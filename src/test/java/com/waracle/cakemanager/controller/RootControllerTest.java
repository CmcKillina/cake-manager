package com.waracle.cakemanager.controller;

import com.waracle.cakemanager.db.Cake;
import com.waracle.cakemanager.db.CakeRepository;
import com.waracle.cakemanager.util.EndpointConstants;
import com.waracle.cakemanager.util.TemplateConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static com.waracle.cakemanager.DataFixture.*;
import static com.waracle.cakemanager.util.ModelConstants.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RootController.class)
public class RootControllerTest {

    private static List<Cake> storedCakeList;

    @MockBean
    private CakeRepository cakeRepositoryMock;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        storedCakeList = new ArrayList<>();
        storedCakeList.add(TEST_CAKE_1);
        storedCakeList.add(TEST_CAKE_2);

        given(cakeRepositoryMock.findAll()).willReturn(storedCakeList);
    }

    @Test
    @DisplayName("Testing get method with no flash attributes adds all Cakes and an input Cake to model")
    void testGetMethodNoFlashAttributes() throws Exception {
        //when + then
        this.mockMvc.perform(get(EndpointConstants.ROOT)
                .accept(MediaType.TEXT_HTML_VALUE))
                .andExpect(status().isOk())
                .andExpect(model().attribute("cakeList", storedCakeList))
                .andExpect(model().attribute(CAKE_ATTR, allOf(
                        hasProperty(TITLE_FIELD, nullValue()),
                        hasProperty(DESCRIPTION_FIELD, nullValue()),
                        hasProperty(IMAGE_FIELD, nullValue())
                )))
                .andExpect(flash().attribute("submittedCake", nullValue()));
    }

    @Test
    @DisplayName("Testing get method with flash attribute adds all Cakes, an input cake and a submitted cake to model")
    void testGetMethodWithFlashAttributes() throws Exception {
        //when + then
        this.mockMvc.perform(get(EndpointConstants.ROOT)
                .accept(MediaType.TEXT_HTML_VALUE)
                .flashAttr(SUBMITTED_CAKE_ATTR, TEST_CAKE_2))
                .andExpect(status().isOk())
                .andExpect(model().attribute("cakeList", storedCakeList))
                .andExpect(model().attribute(CAKE_ATTR, allOf(
                        hasProperty(TITLE_FIELD, nullValue()),
                        hasProperty(DESCRIPTION_FIELD, nullValue()),
                        hasProperty(IMAGE_FIELD, nullValue())
                )))
                .andExpect(model().attribute(SUBMITTED_CAKE_ATTR, allOf(
                        hasProperty(TITLE_FIELD, equalTo(TEST_CAKE_2_TITLE)),
                        hasProperty(DESCRIPTION_FIELD, equalTo(TEST_CAKE_2_DESCRIPTION)),
                        hasProperty(IMAGE_FIELD, equalTo(TEST_CAKE_2_IMAGE))
                )));
    }

    @Test
    @DisplayName("Testing post with null params in returns model with NotNull validation errors")
    void testPostWithNullValidationErrors() throws Exception {
        //when + then
        this.mockMvc.perform(post(EndpointConstants.ROOT))
                .andExpect(status().isOk())
                .andExpect(view().name(TemplateConstants.ROOT_TEMPLATE))
                .andExpect(model().attributeHasFieldErrorCode(CAKE_ATTR, TITLE_FIELD, "NotNull"))
                .andExpect(model().attributeHasFieldErrorCode(CAKE_ATTR, DESCRIPTION_FIELD, "NotNull"))
                .andExpect(model().attributeHasFieldErrorCode(CAKE_ATTR, IMAGE_FIELD, "NotNull"))
                .andExpect(model().attribute(CAKE_ATTR, allOf(
                        hasProperty(TITLE_FIELD, nullValue()),
                        hasProperty(DESCRIPTION_FIELD, nullValue()),
                        hasProperty(IMAGE_FIELD, nullValue())
                )));
    }

    @Test
    @DisplayName("Testing post with strings exceeding min length returns model with Size validation errors")
    void testPostWithMinLengthStringValidationErrors() throws Exception {
        //given

        //when + then
        this.mockMvc.perform(post(EndpointConstants.ROOT)
                .param(TITLE_FIELD, "")
                .param(DESCRIPTION_FIELD, "")
                .param(IMAGE_FIELD, ""))
                .andExpect(status().isOk())
                .andExpect(view().name(TemplateConstants.ROOT_TEMPLATE))
                .andExpect(model().attributeHasFieldErrorCode(CAKE_ATTR, TITLE_FIELD, "Size"))
                .andExpect(model().attributeHasFieldErrorCode(CAKE_ATTR, DESCRIPTION_FIELD, "Size"))
                .andExpect(model().attributeHasFieldErrorCode(CAKE_ATTR, IMAGE_FIELD, "Size"))
                .andExpect(model().attribute(CAKE_ATTR, allOf(
                        hasProperty(TITLE_FIELD, emptyString()),
                        hasProperty(DESCRIPTION_FIELD, emptyString()),
                        hasProperty(IMAGE_FIELD, emptyString())
                )));
    }

    @Test
    @DisplayName("Testing post with strings exceeding max length returns model with Size validation errors")
    void testPostWithMaxLengthStringValidationErrors() throws Exception {
        //given
        String stringGreaterThan100Chars = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        String stringGreaterThan300Chars = stringGreaterThan100Chars + stringGreaterThan100Chars + stringGreaterThan100Chars;

        //when + then
        this.mockMvc.perform(post(EndpointConstants.ROOT)
                .param(TITLE_FIELD, stringGreaterThan100Chars)
                .param(DESCRIPTION_FIELD, stringGreaterThan100Chars)
                .param(IMAGE_FIELD, stringGreaterThan300Chars))
                .andExpect(status().isOk())
                .andExpect(view().name(TemplateConstants.ROOT_TEMPLATE))
                .andExpect(model().attributeHasFieldErrorCode(CAKE_ATTR, TITLE_FIELD, "Size"))
                .andExpect(model().attributeHasFieldErrorCode(CAKE_ATTR, DESCRIPTION_FIELD, "Size"))
                .andExpect(model().attributeHasFieldErrorCode(CAKE_ATTR, IMAGE_FIELD, "Size"))
                .andExpect(model().attribute(CAKE_ATTR, allOf(
                        hasProperty(TITLE_FIELD, equalTo(stringGreaterThan100Chars)),
                        hasProperty(DESCRIPTION_FIELD, equalTo(stringGreaterThan100Chars)),
                        hasProperty(IMAGE_FIELD, equalTo(stringGreaterThan300Chars))
                )));
    }

    @Test
    @DisplayName("Testing post with title already present in DB")
    void testPostWithTitlePresentInDBValidationErrors() throws Exception {
        //given
        given(cakeRepositoryMock.existsCakeByTitle(anyString())).willReturn(true);

        String duplicateTitle = "duplicateTitle";
        String validDescription = "description";
        String validImage = "image";

        //when + then
        this.mockMvc.perform(post(EndpointConstants.ROOT)
                .param(TITLE_FIELD, duplicateTitle)
                .param(DESCRIPTION_FIELD, validDescription)
                .param(IMAGE_FIELD, validImage))
                .andExpect(status().isOk())
                .andExpect(view().name(TemplateConstants.ROOT_TEMPLATE))
                .andExpect(model().attributeHasFieldErrors(CAKE_ATTR, TITLE_FIELD))
                .andExpect(model().attribute(CAKE_ATTR, allOf(
                        hasProperty(TITLE_FIELD, equalTo(duplicateTitle)),
                        hasProperty(DESCRIPTION_FIELD, equalTo(validDescription)),
                        hasProperty(IMAGE_FIELD, equalTo(validImage))
                )));
    }

    @Test
    @DisplayName("Testing post valid Cake entity")
    void testPostValidCakeParams() throws Exception {
        //given
        given(cakeRepositoryMock.existsCakeByTitle(anyString())).willReturn(false);
        given(cakeRepositoryMock.save(ArgumentMatchers.any())).willReturn(TEST_CAKE_1);

        String validTitle = "validTitle";
        String validDescription = "validDescription";
        String validImage = "validImage";

        //when + then
        this.mockMvc.perform(post(EndpointConstants.ROOT)
                .param(TITLE_FIELD, validTitle)
                .param(DESCRIPTION_FIELD, validDescription)
                .param(IMAGE_FIELD, validImage))
                .andExpect(status().isMovedTemporarily())
                .andExpect(view().name("redirect:" + EndpointConstants.ROOT))
                .andExpect(flash().attribute(SUBMITTED_CAKE_ATTR, TEST_CAKE_1));
    }
}
