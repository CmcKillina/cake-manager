package com.waracle.cakemanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemanager.db.Cake;
import com.waracle.cakemanager.db.CakeRepository;
import com.waracle.cakemanager.util.EndpointConstants;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static com.waracle.cakemanager.DataFixture.TEST_CAKE_1;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CakesController.class)
public class CakesControllerTest {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private CakeRepository cakeRepositoryMock;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Testing download method returns response with correct header and expected cake list in JSON format in body")
    public void testDownloadReturnsExpectedList() throws Exception {
        //given
        List<Cake> storedCakeList = new ArrayList<>();
        storedCakeList.add(TEST_CAKE_1);

        given(cakeRepositoryMock.findAll()).willReturn(storedCakeList);

        //when + then
        this.mockMvc.perform(get(EndpointConstants.CAKES)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"cakes.json\""))
                .andExpect(content().string(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(storedCakeList)));
    }

    @Test
    @DisplayName("Testing add method returns Conflict status when a Cake already exists in the DB with the requested title")
    public void testAddReturnsConflictResponseWhenTitleAlreadyExists() throws Exception {
        //given
        given(cakeRepositoryMock.existsCakeByTitle(anyString())).willReturn(true);

        //when + then
        this.mockMvc.perform(post(EndpointConstants.CAKES)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(TEST_CAKE_1)))
                .andExpect(status().isConflict());
    }

    @Test
    @DisplayName("Testing add method returns Created status when a Cake is successfully added to the DB")
    public void testAddReturnsCreatedResponseWhenCakeIsAddedToDB() throws Exception {
        //given
        given(cakeRepositoryMock.existsCakeByTitle(anyString())).willReturn(false);
        given(cakeRepositoryMock.save(any())).willReturn(TEST_CAKE_1);

        //when + then
        this.mockMvc.perform(post(EndpointConstants.CAKES)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(TEST_CAKE_1)))
                .andExpect(status().isCreated());
    }

    @Test
    @DisplayName("Testing add method returns Created status when a Cake is successfully added to the DB")
    public void test() throws Exception {
        //given
        given(cakeRepositoryMock.existsCakeByTitle(anyString())).willReturn(false);
        given(cakeRepositoryMock.save(any())).willReturn(TEST_CAKE_1);

        //when + then
        this.mockMvc.perform(post(EndpointConstants.CAKES)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(TEST_CAKE_1)))
                .andExpect(status().isCreated());
    }
}
