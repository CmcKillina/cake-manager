package com.waracle.cakemanager.util;

public final class TemplateConstants {

    public static final String ROOT_TEMPLATE = "root";

    private TemplateConstants() {
        //Utility class
    }
}
