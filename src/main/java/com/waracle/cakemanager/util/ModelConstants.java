package com.waracle.cakemanager.util;

public final class ModelConstants {

    // Attributes
    public static final String SUBMITTED_CAKE_ATTR = "submittedCake";
    public static final String CAKE_ATTR = "cake";

    // Fields
    public static final String TITLE_FIELD = "title";
    public static final String DESCRIPTION_FIELD = "description";
    public static final String IMAGE_FIELD = "image";

    // Error Messages
    public static final String TITLE_ALREADY_EXIST_ERROR_MESSAGE = "Cake with title %s already exists, please use another.";

    private ModelConstants() {
        //Utility class
    }
}
