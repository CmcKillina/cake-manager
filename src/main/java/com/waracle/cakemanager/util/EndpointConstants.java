package com.waracle.cakemanager.util;

public class EndpointConstants {

    public static final String ROOT = "/";
    public static final String CAKES = "/cakes";

    private EndpointConstants() {
        //Utility class
    }
}
