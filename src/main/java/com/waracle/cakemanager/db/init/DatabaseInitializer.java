package com.waracle.cakemanager.db.init;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemanager.db.Cake;
import com.waracle.cakemanager.db.CakeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.dao.DataIntegrityViolationException;

import javax.persistence.PersistenceException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

@Configuration
public class DatabaseInitializer {

    private static final Logger logger = LoggerFactory.getLogger(DatabaseInitializer.class);

    @Value("${databaseInitializer.url}")
    private String cakesDownloadUrl;

    private final ResourceLoader resourceLoader;

    public DatabaseInitializer(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Bean
    CommandLineRunner init(CakeRepository cakeRepository) {
        return args -> {
            Resource resource = resourceLoader.getResource(cakesDownloadUrl);

            try (InputStream inputStream =  resource.getInputStream()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuilder buffer = new StringBuilder();
                String line = reader.readLine();
                while (line != null) {
                    buffer.append(line);
                    line = reader.readLine();
                }

                ObjectMapper objectMapper = new ObjectMapper();
                Cake[] cakeList = objectMapper.readValue(buffer.toString(), Cake[].class);

                logger.info("Loading entries into database.");
                Arrays.stream(cakeList).forEach(cake -> {
                    try {
                        Cake saveResult = cakeRepository.save(cake);
                        logger.info(String.format("Successfully added to database: %s", saveResult));
                    } catch (DataIntegrityViolationException e) {
                        logger.warn(String.format("Failed to add %s to database. Integrity constraint has been violated.", cake));
                    } catch (PersistenceException e) {
                        logger.error(String.format("Failed to add %s to database", cake), e);
                    }
                });
                logger.info("Database loading complete.");
            } catch (Exception ex) {
                logger.warn(String.format("Unable to retrieve initial list of cakes from URL %s. Skipping db populating.", cakesDownloadUrl), ex);
            }
        };
    }
}
