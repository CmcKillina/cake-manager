package com.waracle.cakemanager.db;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@DynamicUpdate
public class Cake {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Size(
        min = 1,
        max = 100,
        message = "The title must be between {min} and {max} characters long"
    )
    @Column(unique = true)
    private String title;

    @NotNull
    @Size(
        min = 1,
        max = 100,
        message = "The description must be between {min} and {max} characters long"
    )
    @Column
    @JsonProperty("desc")
    private String description;

    @NotNull
    @Size(
        min = 1,
        max = 300,
        message = "The image URL must be between {min} and {max} characters long"
    )
    @Column
    private String image;

    public Cake() {
    }

    public Cake(String title, String description, String image) {
        this.title = title;
        this.description = description;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Cake{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}