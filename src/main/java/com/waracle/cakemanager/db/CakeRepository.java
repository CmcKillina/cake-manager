package com.waracle.cakemanager.db;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CakeRepository extends JpaRepository<Cake, Integer> {

    boolean existsCakeByTitle(String title);
}
