package com.waracle.cakemanager.controller;

import com.waracle.cakemanager.db.Cake;
import com.waracle.cakemanager.db.CakeRepository;
import com.waracle.cakemanager.util.EndpointConstants;
import com.waracle.cakemanager.util.ModelConstants;
import com.waracle.cakemanager.util.TemplateConstants;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;

@Controller(EndpointConstants.ROOT)
public class RootController {

    private final CakeRepository cakeRepository;

    public RootController(CakeRepository cakeRepository) {
        this.cakeRepository = cakeRepository;
    }

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    protected String get(Model model, HttpServletRequest httpServletRequest) {
        addCakesToModel(model);

        addFlashAttributesToModel(model, httpServletRequest);

        model.addAttribute(ModelConstants.CAKE_ATTR, new Cake());

        return TemplateConstants.ROOT_TEMPLATE;
    }

    @PostMapping
    protected String post(@Valid @ModelAttribute Cake cake,
                          BindingResult bindingResult,
                          Model model,
                          RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            addCakesToModel(model);

            return TemplateConstants.ROOT_TEMPLATE;
        }

        if (cakeRepository.existsCakeByTitle(cake.getTitle())) {
            String errorMessage = String.format(ModelConstants.TITLE_ALREADY_EXIST_ERROR_MESSAGE, cake.getTitle());
            bindingResult.addError(new FieldError(ModelConstants.CAKE_ATTR, ModelConstants.TITLE_FIELD, errorMessage));

            addCakesToModel(model);

            return TemplateConstants.ROOT_TEMPLATE;
        }

        Cake savedCake = cakeRepository.save(cake);

        redirectAttributes.addFlashAttribute(ModelConstants.SUBMITTED_CAKE_ATTR, savedCake);

        return "redirect:" + EndpointConstants.ROOT;
    }

    private void addFlashAttributesToModel(Model model, HttpServletRequest httpServletRequest) {
        Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(httpServletRequest);

        if (inputFlashMap != null) {
            Cake submittedCake = (Cake) inputFlashMap.get(ModelConstants.SUBMITTED_CAKE_ATTR);
            model.addAttribute(submittedCake);
        }
    }

    private void addCakesToModel(Model model) {
        model.addAttribute(cakeRepository.findAll());
    }
}
