package com.waracle.cakemanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemanager.db.Cake;
import com.waracle.cakemanager.db.CakeRepository;
import com.waracle.cakemanager.util.EndpointConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping(EndpointConstants.CAKES)
public class CakesController {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private final CakeRepository cakeRepository;

    public CakesController(CakeRepository cakeRepository) {
        this.cakeRepository = cakeRepository;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    protected ResponseEntity<String> download(HttpServletResponse httpServletResponse) throws IOException {
        List<Cake> cakes = cakeRepository.findAll();

        httpServletResponse.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"cakes.json\"");

        String jsonString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(cakes);

        return ResponseEntity.status(HttpStatus.OK).body(jsonString);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    protected ResponseEntity<?> add(@Valid @RequestBody Cake cake) {
        if (cakeRepository.existsCakeByTitle(cake.getTitle())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        cakeRepository.save(cake);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
