Cake Manager Micro Service (fictitious)
=======================================

A summer intern started on this project but never managed to get it finished.
The developer assured us that some of the above is complete, but at the moment accessing the /cakes endpoint
returns a 404, so getting this working should be the first priority.

Requirements:
* By accessing the root of the server (/) it should be possible to list the cakes currently in the system. This must be presented in an acceptable format for a human to read.

* It must be possible for a human to add a new cake to the server.

* By accessing an alternative endpoint (/cakes) with an appropriate client it must be possible to download a list of
the cakes currently in the system as JSON data.

* The /cakes endpoint must also allow new cakes to be created.

Comments:
* We feel like the software stack used by the original developer is quite outdated, it would be good to migrate the entire application to something more modern.
* Would be good to change the application to implement proper client-server separation via REST API.

Bonus points:
* Tests
* Authentication via OAuth2
* Continuous Integration via any cloud CI system
* Containerisation


Original Project Info
=====================

To run a server locally execute the following command:

`mvn jetty:run`

and access the following URL:

`http://localhost:8282/`

Feel free to change how the project is run, but clear instructions must be given in README
You can use any IDE you like, so long as the project can build and run with Maven or Gradle.

The project loads some pre-defined data in to an in-memory database, which is acceptable for this exercise.  There is
no need to create persistent storage.

Updated Project Info
====================

## Run the application

### Method 1: Maven

The application has now been migrated to Spring. To run the server please execute the following maven command:

`mvn spring-boot:run`

### Method 2: Docker 

You also run the server using Docker locally by running in the root of the project. First build the image by calling

`docker build -t waracle/cake-manager .`

and then you can run the container by calling

`docker run -p8080:8080 waracle/cake-manager`

## Endpoints

Once running, you can now access the root of the server at 

`http://localhost:8080`

The root endpoint serves up a HTML page with a human-readable list of cakes in the system. The page also provides a form
to add more cakes to the system. The form data is validated and feedback is provided in the form of error messages on the 
form input fields if any data fails validation.

To download a list of all the cakes in the system you can make a GET request to the `/cakes` endpoint with an appropriate client (for e.g. Browser)

`http://localhost:8080/cakes`

If the request is successful, the server will return a 200 OK response with the JSON in the response body and a header 
of `content-disposition : "attachment; filename=\"cakes.json\""`.

You can also add cakes to the system via the `/cakes` endpoint by making a POST request to the endpoint.

```
POST http://localhost:8080/cakes
content-type: application/json
 
 {
   "title" : "Example title",\
   "desc" : "Example description",
   "image" : "Example image"
 }
```

If successful the server will send back a `201 CREATED` response. If the data passed in the request body does not pass the validation
then a `400 BAD_REQUEST` response is returned. Details of the validation failure is included in the response body.

## Continuous Integration [![Build Status](https://dev.azure.com/mccullochcallum/callums-development-dashboard/_apis/build/status/CmcKillina.cake-manager?branchName=master&jobName=Build)](https://dev.azure.com/mccullochcallum/callums-development-dashboard/_build/latest?definitionId=3&branchName=master)

Azure Pipelines has been utilised for CI. You can view the build dashboard [here](https://dev.azure.com/mccullochcallum/callums-development-dashboard/_build?definitionId=3&view=runs).
The pipeline is triggered on creation/update to PR on master and also on commit to master. 

Submission
==========

Please provide your version of this project as a git repository (e.g. Github, BitBucket, etc).

Alternatively, you can submit the project as a zip or gzip. Use Google Drive or some other file sharing service to
share it with us.

Please also keep a log of the changes you make as a text file and provide this to us with your submission.

Good luck!
